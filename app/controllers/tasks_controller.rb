class TasksController < ApplicationController
  before_action :find_tasks, only: [:index, :new, :create]
  respond_to :html, :js, :json

  def new
    @task = Task.new
  end

  def create
    @task = Task.create(task_params)
    StatusJob.set(wait: 5.days).perform_later(@task)
  end

  def update
  end

  private

  def find_tasks
    @tasks = Task.all
  end

  def task_params
    params.require(:task).permit(:name, :start_day, :end_day)
  end
end
