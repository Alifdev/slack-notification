# == Schema Information
#
# Table name: tasks
#
#  id         :integer          not null, primary key
#  name       :string
#  start_day  :datetime
#  end_day    :datetime
#  status     :integer          default("defualt")
#  state      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Task < ApplicationRecord
  enum status: { default: 0, accept: 1, in_process: 2, solve: 3 }

  scope :quite_old, -> { where(status: 0).where('start_day < ?', 5.days.ago) }

  def self.update_old_tasks
    if quite_old.count > 0
      slack_message
      quite_old.update_all(status: 3)
    end
  end

  def self.slack_message
    slack_notifier = 'Last week task ' + " ``` #{quite_old.map(&:name).join(',')} ``` " + ' was updated'

    Slack::Notifier.new('https://hooks.slack.com/services/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', username: 'Notifier').ping(slack_notifier)
  end
end
