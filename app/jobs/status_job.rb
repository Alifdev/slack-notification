class StatusJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Task.update_old_tasks
  end
end
