class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.datetime :start_day
      t.datetime :end_day
      t.integer :status, default: 0
      t.string :state

      t.timestamps
    end
  end
end
